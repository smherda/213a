
					hw1_prob10.py

### README
The following is for the hw1_prob10.py script. The code should be compiled in a Python version 3.0 or above 
with graphics that can run the MATLAB package. The code is designed to create graphs which are used in Problem 
#10 in Homework #1. 

### Installation
Please have the matplotlib package installed. This can be done through running the following Python command 
in the console: 
pip install matplotlib

### Contributing 
Pull requests are welcome. 

### Author
Stacie Herda

### License
[UCSC](https://www.ucsc.edu/)