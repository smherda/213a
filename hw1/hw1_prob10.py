#####INFORMATION
#Name:StacieHerda
#Class:213aLinearAlgebra
#Quarter:Winter2021
#DateSubmitted:1/27/2021
#Title:Hw#1Prob#10

#####CODE

#####Prob10a
#ImportLibraries
import matplotlib.pyplot as plt

#GetArrayforDiscreteValuesforFunction
#GoUpto2081SinceLastPointisRemovedandWant2.080asLastPoint
f_x=list(range(1920,2081,1)) 

#Divideby1000SinceRangeOnlyTakesFloatsInsteadofIntegers
f_x=[num/1000 for num in f_x]

#InputIntoFunction
f_y=[(num-2)**9 for num in f_x]

#CreateGraphforParta
fig=plt.figure()
plt.plot(f_x,f_y,'b.')
plt.title('Evalution of f(x)')
plt.xlabel('x')
plt.ylabel('y')

#####Prob10b
#GeneratePointsfortheEquation
g_x=f_x
g_y=[num**9-18*num**8+144*num**7-672*num**6+2016*num**5-4032*num**4+
     5376*num**3-4608*num**2+2304*num-512
     for num in g_x]

#PlotFunction:g(x)
fig=plt.figure()
plt.plot(f_x,f_y,'b.',)
plt.title('Evalution of f(x) and g(x)')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(g_x,g_y,'g.')

#PrintExampleofx=2.01
f_y=[(num-2)**9 for num in [2.02]]
g_y=[num**9-18*num**8+144*num**7-672*num**6+2016*num**5-4032*num**4+
     5376*num**3-4608*num**2+2304*num-512
     for num in [2.02]]

print('f(x)=',f_y)
print('g(x)=',g_y)
