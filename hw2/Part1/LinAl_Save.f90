!ProvidedCodeBelow
module LinAl
  implicit none  
  integer,save::msize,nsize;
  real,dimension(:,:),allocatable,save::mat
contains
  subroutine readMat(filename,mat,msize,nsize)
    implicit none
    character(len=*)::filename
    integer::i,j
    allocate(mat(i,j))
    open(10,file=filename)
    read(10,*) i,j
    do i=1,msize
       read(10,*) (mat(i,j),j=1,nsize)
    enddo
    close(10)    
  end subroutine readMat
end module LinAl
!EndofProvidedCode
